package com.LHV.LHV;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.swing.*;
import java.io.*;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

@EnableScheduling
@SpringBootApplication
public class StockDataApplication {

    static SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-mm-dd");

    static SimpleDateFormat dFormatFinal = new SimpleDateFormat("dd-mm-yyyy");

    @Scheduled(fixedRate = 1800000)
    public static void updateInformation() { // update information every 1800000 milliseconds(30 minutes)

        deleteToday(); //delete current day information
        checkFiles(); 
    }

    public static void download(String date) throws Exception {

        String fileName = "./dates/" + date.replace('.', '-') + ".csv";
        String url = "http://www.nasdaqbaltic.com/market/?pg=mainlist&market=&date=" + date + "&lang=en&downloadcsv=1&csv_style=Baltic";
        File file = new File(fileName);
        if (file.exists()) { 
            System.out.println(fileName + " exists");
        } else { // download information about date that does not exist
            System.out.println("Downloading " + fileName);
            try (InputStream in = URI.create(url).toURL().openStream()) {
                Files.copy(in, Paths.get(fileName));

            }

        }

    }

    public static ArrayList<String> getLastTenDays() {
        LocalDate today = LocalDate.now();
        ArrayList<String> days = new ArrayList<>();
        StockForm.listModel.removeAllElements();

        for (int i = 0; i < 10; i++) {
            
            String date = convertDate(today.plusDays(-i).toString()); // convert date to correct format
            days.add(date);
            Runnable aRunnable = new Runnable() {
                public void run() {
                    StockForm.addDate(date);
                }
            };
            SwingUtilities.invokeLater(aRunnable);

        }

        return days;
    }

    public static String convertDate(String dateString) {
        Date date = null;
        try {
            date = dFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String convertedDate = dFormatFinal.format(date);

        return convertedDate;
    }

    public static void checkFiles() {

        ArrayList<String> downloadedDays = new ArrayList<>();
        downloadedDays.clear();
        File directory = new File("./dates");
        //get all files from directory
        File[] fList = directory.listFiles();
        for (File file : fList) {

            String name = file.getName();
            if (name.endsWith(".csv")) {

                String[] info = file.getName().split("\\.");
                System.out.println(info[0]);
                downloadedDays.add(info[0]); // add information about day that exists

            }
        }

        ArrayList<String> days = getLastTenDays(); // get last 10 days starting from today

        //delete old files
        for (int i = 0; i < downloadedDays.size(); i++) {

            if (!days.contains(downloadedDays.get(i))) { // delete information that is older than 10 days
                File file = new File("./dates/" + downloadedDays.get(i) + ".csv");
                File picture = new File("./dates/" + downloadedDays.get(i) + ".jpg");
                System.out.println("deleteing "+downloadedDays.get(i));
                
                
                if (picture.delete()) { // delete picture
                    System.out.println(picture.getName() + " is deleted!");
                } else {
                    System.out.println("Delete operation is failed.");
                }
                
                if (file.delete()) { // delete file
                    System.out.println(file.getName() + " is deleted!");
                } else {
                    System.out.println("Delete operation failed");
                }
            }
        }
        
       

        for (int i = 0; i < days.size(); i++) {
            try {
                download(days.get(i)); // download new information
            } catch (Exception e) {
                
            }
        }

    }

    public static void deleteToday() {
        LocalDate today = LocalDate.now();
        System.out.println(convertDate(today.toString()));
        File file = new File("./dates/" + convertDate(today.toString()) + ".csv");

        if (file.delete()) {
            System.out.println(file.getName() + " is deleted!");
        } else {
            System.out.println("Delete operation failed");
        }
    }
}
